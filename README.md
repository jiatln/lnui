# Lnui

> 基于 Vue 2.0 的桌面端组件库

## npm 安装

推荐使用 npm 的方式安装，它能更好地和 webpack 打包工具配合使用。

```
npm install @jiatln/lnui -S
```

## 引入 LnUI

### 完整引入

在 main.js 中写入以下内容：

```js
import Vue from 'vue';
import LnUI from '@jiatln/lnui';
import '@jiatln/lnui/lib/lnui.css';
import App from './App.vue';

Vue.use(LnUI);

new Vue({
  el: '#app',
  render: h => h(App),
});
```

## 如何使用

各个组件的使用方法请参阅它们各自的文档。
