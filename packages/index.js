import Avatar from './Avatar/index';
import Button from './Button/index';
import Breadcrumb from './Breadcrumb/index';
import BreadcrumbItem from './BreadcrumbItem/index';
import Divider from './Divider/index';
import List from './List/index';
import ListItem from './ListItem/index';
import NumCard from './NumCard/index';

const components = [Avatar, Button, Breadcrumb, BreadcrumbItem, Divider, List, ListItem, NumCard];

const install = Vue => {
  if (install.installed) return;
  components.map(component => Vue.component(component.name, component));
};

if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue);
}

export default {
  install,
  Avatar,
  Breadcrumb,
  BreadcrumbItem,
  Button,
  Divider,
  List,
  ListItem,
  NumCard,
};
