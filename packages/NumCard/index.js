import NumCard from './src/NumCard.vue';

NumCard.install = function (Vue) {
  Vue.component(NumCard.name, NumCard);
};

export default NumCard;
