import Divider from './src/Divider.vue';

Divider.install = function (Vue) {
  Vue.component(Divider.name, Divider);
};

export default Divider;
