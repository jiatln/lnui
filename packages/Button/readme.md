## 按钮组件

```html
<ln-button></ln-button>
```

### Attributes

| 参数   | 说明             | 类型      | 可选项                                         | 默认值    | 必选 |
| ------ | ---------------- | --------- | ---------------------------------------------- | --------- | ---- |
| type   | 按钮类型         | `String`  | `primary` \| `success` \| `warning` \| `error` | `primary` | 否   |
| round  | 是否开启圆角     | `Boolean` | `true` \| `false`                              | `false`   | 否   |
| block  | 是否块状按钮     | `Boolean` | `true` \| `false`                              | `false`   | 否   |
| ripple | 是否点击水波效果 | `Boolean` | `true` \| `false`                              | `true`    | 否   |
| size   | 尺寸             | `String`  | `small` \| `normal` \| `large`                 | `normal`  | 否   |
