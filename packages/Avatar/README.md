## 头像组件

```html
<ln-avatar></ln-avatar>
```

### Attributes

| 参数      | 说明     | 类型   | 可选项               | 默认值    | 必选 |
| --------- | -------- | ------ | -------------------- | --------- | ---- |
| size      | 头像尺寸 | Number | -                    | `44`      | 否   |
| color     | 背景色   | String | -                    | `#bc261b` | 否   |
| textColor | 文本颜色 | String | -                    | `#fff`    | 否   |
| shape     | 头像形状 | String | `circle` \| `square` | `circle`  | 否   |
