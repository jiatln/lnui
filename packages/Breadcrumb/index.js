import Breadcrumb from './src/Breadcrumb.vue';

Breadcrumb.install = function (Vue) {
  Vue.component(Breadcrumb.name, Breadcrumb);
};

export default Breadcrumb;
