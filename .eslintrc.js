module.exports = {
  // 使用当前配置文件设置eslint
  root: true,
  env: {
    node: true,
  },
  extends: ['plugin:vue/essential', 'eslint:recommended', '@vue/prettier'],
  // eslint解析器
  parserOptions: {
    parser: 'babel-eslint',
  },
  // "off" 或 0 - 关闭规则
  // "warn" 或 1 - 开启规则，使用警告级别的错误：warn (不会导致程序退出)
  // "error" 或 2 - 开启规则，使用错误级别的错误：error (当被触发的时候，程序会退出)
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'off' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-var': 'warn', // 要求使用 let 或 const 而不是 var
    eqeqeq: 'warn', // 要求使用 === 和 !==
    'default-case': 'warn', // 要求 switch 语句中有 default 分支
    'no-use-before-define': 'warn', // 禁止在变量定义之前使用它们
    'no-duplicate-case': 'warn', // 禁止出现重复的 case 标签
    'no-empty': 'warn', // 禁止出现空语句块
    'no-redeclare': 'warn', // 禁止多次声明同一变量
    'no-trailing-spaces': 'warn', // 禁用行尾空格
    'space-infix-ops': 'warn', // 要求操作符周围有空格
    'spaced-comment': 'warn', // 强制在注释中 // 或 /* 使用一致的空格
    'no-useless-return': 'warn', // 禁止多余的 return 语句
    'no-lone-blocks': 'warn', // 禁用不必要的嵌套块
    'array-bracket-spacing': 'warn', // 强制数组方括号中使用一致的空格
  },
};
