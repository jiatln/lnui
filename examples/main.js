import Vue from 'vue';
import App from './App.vue';

import 'normalize.css/normalize.css';
import './../packages/style/reset.less';
import './../packages/style/com.less';

import LnUI from './../packages/index';

Vue.use(LnUI);

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount('#app');
